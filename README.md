# Kunskapsmatrisskapare

En matrisskapare som skapar kunskapsmatriser i word för varje elev i en grupp utifrån ett excelark. Det finns också ett skript för att automatiskt maila ut matrisen till eleverna med personlig eller övergripande feedback.

## Installation och körning
**Installation**
```python
pip install python-docx
pip install openpyxl
```
**Körning**
```python
python pythoncreatematris.py
python sendmails
```

## Att tänka på
Att tänka på, matriser skickas inte iväg om eleven har ÅÄÖé och andra udda tecken i sitt namn. Så gör om dem till läsbara tecken innan filerna körs.

## SplitOutlookmailgroup
Skapar två textfiler
1. Namn
2. Adresser

Den ena innehåller alla namn från maillistan den andra innehåller mailadresserna. Detta för man enkelt ska kunna klistra in dem i ett excelark.
Den läser maillistor fron MailadressesfromOutlook.txt

## pythoncreatematris
Skapar wordfiler med kunskapsmatriser för varje elev i kunskapsmatrisfliken i matris.xslx.

## sendmails
SKickar mail till eleverna som ligger i kunskapsmatrisfliken i matris.xslx. Har man en personlig feedback i feedback fliken skickas den med annars skickas standardsvaret från Infofliken.

