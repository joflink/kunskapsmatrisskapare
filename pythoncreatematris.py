from docx import Document #för att hantera dokument
import os #För att skapa mapar
from docx.shared import Cm #För att göra loggan mindre
import openpyxl #För att hantera excelfiler
import datetime #Läsa av tid och dag

date_object = datetime.date.today()
# wb= openpyxl.load_workbook('Bedömning_prog.xlsx')

wb= openpyxl.load_workbook('matris.xlsx')

#Visar alla sheetname
# print(wb.get_sheet_names())
#Aktuell info
info= wb.get_sheet_by_name('Info')
lärarnamn=info.cell(1,2).value
Ämne=info.cell(2,2).value
Klass=info.cell(3,2).value

Centraltinnehåll= wb.get_sheet_by_name('Centralt innehåll')
#Laddar in om elever har genomfört centrala innehållen eller ej
PlatsiCentraltInnehåll=[]
Centralelev= []
for i in range(2,Centraltinnehåll.max_row):
    elevbedömn=[]
    PlatsiCentraltInnehåll.append(Centraltinnehåll.cell(row=i, column=1).value) #Tar ut platsen där namnet ligger på för att få samma matris.
    for p in range(2,Centraltinnehåll.max_column):
        if not Centraltinnehåll.cell(row=i, column=p).value is None:
            elevbedömn.append(Centraltinnehåll.cell(row=i, column=p).value)
    Centralelev.append(elevbedömn)

#Laddar in de centrala innehållen
Centralt=[Centraltinnehåll.cell(row=1, column=i).value
 for i in range(1,Centraltinnehåll.max_column)
  if not Centraltinnehåll.cell(row=1, column=i).value is None ]

 #Läser in kravens texter. E,C och A nivå 
kraven= wb.get_sheet_by_name('kraven')
Kunskapskravbeskrivning=[
    [kraven.cell(row=i, column=1).value,kraven.cell(row=i, column=2).value,kraven.cell(row=i, column=3).value,kraven.cell(row=i, column=4).value]
 for i in range(2,kraven.max_row)
  if not kraven.cell(row=i, column=1).value is None ]


Kunskapskrav= wb.get_sheet_by_name('Kunskapskrav')
#Laddar in alla betygsnivåer eleven har uppnåt i respektive område
# Kunskapskravelev=[{
#         "Namn":Kunskapskrav.cell(row=i, column=1).value,
#         "Planering":Kunskapskrav.cell(row=i, column=2).value,
#         "Syntax":Kunskapskrav.cell(row=i, column=3).value,
#         "Struktur":Kunskapskrav.cell(row=i, column=4).value,
#         "Interaktion":Kunskapskrav.cell(row=i, column=5).value,
#         "Kvalitet":Kunskapskrav.cell(row=i, column=6).value,
#         "Felsökning":Kunskapskrav.cell(row=i, column=7).value,
#         "Utvärdering":Kunskapskrav.cell(row=i, column=8).value,
#         "Diskussion":Kunskapskrav.cell(row=i, column=9).value,
#         "Samhällspåverkan":Kunskapskrav.cell(row=i, column=10).value,
#         "Reflektion":Kunskapskrav.cell(row=i, column=11).value,
# } for i in range(2,Kunskapskrav.max_row)
#   if not Kunskapskrav.cell(row=i, column=1).value is None ]
Kunskapskravelev=[]
for i in range(2,Kunskapskrav.max_row+1):
    if not Kunskapskrav.cell(row=i, column=1).value is None:
        elev= {"Namn":Kunskapskrav.cell(row=i, column=1).value}
        counter=4
        for krav in Kunskapskravbeskrivning:
           elev[krav[0]]= Kunskapskrav.cell(row=i, column=counter).value
           counter+=1
        Kunskapskravelev.append(elev)
  
print(Kunskapskravelev)

#Skapar dokumentet
document = Document()
#Skapar header sektionen där loggan o info ska vara
section = document.sections[0]
header = section.header
paragraph = header.paragraphs[0]
paragraph.text =lärarnamn+"\t"+str(date_object)+"\t"
r = paragraph.add_run()
r.add_picture("ABB-Industrigymnasium_logga.png", width=Cm(6))
document.add_heading('Kunskapsmatris '+Ämne, 0)
#skapar en paragraf där namnet senare ska läggas in
namevar=document.add_heading('', 2)
##Centralt innehåll, skapar en lista där det läggs.
document.add_heading('Centralt innehåll', 1)
cendocarray=[]
for innehåll in Centralt:
    cendocarray.append( document.add_paragraph(
        innehåll, style='List Bullet'
    ))

##Kunskapskrav , skapar en tabell
document.add_heading('Kunskapskrav', 1)
table = document.add_table(rows=1, cols=4)
hdr_cells = table.rows[0].cells
hdr_cells[0].text = 'Rubrik'
hdr_cells[1].text = 'E'
hdr_cells[2].text = 'C'
hdr_cells[3].text = 'A'
for Namn, E, C, A in Kunskapskravbeskrivning:
    row_cells = table.add_row().cells
    row_cells[0].text = Namn
    row_cells[1].text = E
    row_cells[2].text = C
    row_cells[3].text = A

#Skapar mappen där filerna ska sparas, om den inte redan finns.
if(not os.path.exists(str(datetime.date.today().year)+"/"+str(datetime.date.today().month)+"/"+Ämne+"/")):
    os.makedirs(str(datetime.date.today().year)+"/"+str(datetime.date.today().month)+"/"+Ämne+"/")
#För varje elev skapar man ett varsitt dokument med den elevens uppnådda nivåer
for elev in Kunskapskravelev:
    namevar.text='Klass: '+Klass+'\t Namn: '
    counter=0 #Counter används först för att se vilka fonts som ändras i kunskapskravet o senare för samma grej i centrala innehållet
    fonts=[]
    CentralPlace=PlatsiCentraltInnehåll.index(elev["Namn"])
    for bedöm in Centralelev[CentralPlace]: #Om det står genomfört så ska det centrala innehållet bli bold
        # print(bedöm)
        if bedöm == "Genomfört":
            font= cendocarray[counter].runs[0].font
            font.bold=True
            fonts.append(font)
        counter+=1
    counter=0
    # print(elev)
    name=""
    for key, value in elev.items(): #Kollar igenom alla elevens betyg i de olika kunskapskraven och ändrar färgen på det rätta steget.
        if key!="Namn":
            cellnr=6
            if value=="A":
                cellnr=3
            elif value=="C":
                cellnr=2
            elif value=="E":
                cellnr=1
            counter+=1
            if cellnr!=6:
                cell=table.cell(counter,cellnr)
                paragraphs = cell.paragraphs
                for paragraph in paragraphs:
                    for run in paragraph.runs:
                        font = run.font
                        font.bold=True
                        font.highlight_color=4
                        fonts.append(font)
        else:
            namevar.text+=elev["Namn"]
            name=elev["Namn"]  
            #Sparar eleven där den ska vara
    document.save(str(datetime.date.today().year)+"/"+str(datetime.date.today().month)+"/"+Ämne+"/"+name+'.docx')
    for font in fonts: #Återställer värdena till nästa elev.
        font.bold=False
        font.highlight_color=0

