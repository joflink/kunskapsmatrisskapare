import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.mime.base import MIMEBase
import openpyxl #För att hantera excelfiler
import datetime #Läsa av tid och dag

#Öppna excelfil
wb= openpyxl.load_workbook('matris.xlsx')

info= wb.get_sheet_by_name('Info')
lärarnamn=info.cell(1,2).value
Ämne=info.cell(2,2).value
Klass=info.cell(3,2).value
Text=info.cell(4,2).value
Rubrik=info.cell(5,2).value
fromMail=info.cell(6,2).value
Password=info.cell(7,2).value
# print(fromMail,Password)

Kunskapskrav= wb.get_sheet_by_name('Kunskapskrav')
NamnMailElev=[]
for i in range(2,Kunskapskrav.max_row+1):
    if not Kunskapskrav.cell(row=i, column=1).value is None:
        elev= {"Namn":Kunskapskrav.cell(row=i, column=1).value,"Mail":Kunskapskrav.cell(row=i, column=2).value,"Message":Kunskapskrav.cell(row=i, column=3).value}
        NamnMailElev.append(elev)
# print(NamnMailElev)


server = smtplib.SMTP('SMTP.Office365.com', 587) #Detta är office mail, googles har en annan osv.
server.connect("SMTP.Office365.com",587)
server.ehlo()
server.starttls()
#Next, log in to the server
server.login(fromMail, Password)
# server.starttls()


for elev in NamnMailElev:
  print(elev)
  filename= str(datetime.date.today().year)+"/"+str(datetime.date.today().month)+"/"+Ämne+"/"+elev["Namn"]+'.docx'
  print(filename)


  message = MIMEMultipart("alternative")
  message["Subject"] = Rubrik
  message["From"] = fromMail
  message["To"] =  elev["Mail"]
  message["Bcc"] = fromMail  # Recommended for mass emails


  #Attached filr 
  # filename = "test.docx"  # In same directory as script
  # Open PDF file in binary mode
  with open(filename, "rb") as attachment:
      # Add file as application/octet-stream
      # Email client can usually download this automatically as attachment
      part = MIMEBase("application", "octet-stream")
      part.set_payload(attachment.read())

  # Encode file in ASCII characters to send by email    
  encoders.encode_base64(part)

  # Add header as key/value pair to attachment part
  part.add_header(
      "Content-Disposition",
      f"attachment; filename= {filename}",
  )
  message.attach(part)
  student=elev["Namn"].split(" ")[0]
  if len(student)==0:
    student=elev["Namn"].split(" ")[1]
  # print(student)
  # Text.format(elev["Namn"])



               
  if elev["Message"]is not None:
    print("Eget Meddelande")
    mess=elev["Message"]
    Messpart = MIMEText(mess, "plain")
    message.attach(Messpart)
  else:
      #Message
    NyText=Text.replace("%Namn", student)
    # print(NyText)
    Messagetext= "<html><body>"
    Messagetext+= NyText+"    </body>  </html>"
    Messagetext.format(student=elev["Namn"])
    Messpart = MIMEText(Messagetext, "html")
    message.attach(Messpart)

  server.send_message(message)
  del message